### Test Coverage

[![Django report](https://gitlab.com/anastasia_greta/web-ppw/badges/master/coverage.svg)](https://gitlab.com/WebDev-Google/crowd-funding-f1/commits/master)

### Pipeline Status

![Pipeline status](https://gitlab.com/anastasia_greta/web-ppw/badges/master/build.svg)