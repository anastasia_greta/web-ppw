$(document).ready(function () {
    // Accordion
    $('.item').click(function () {
        $(this).next().slideToggle(300);
        $('p').not($(this).next()).slideUp('fast');
    });

    // Change Theme
    $(".theme").click(function(){
        $(".topnav").toggleClass('topnav2');
        $(".fonth1").toggleClass('fonth12');
        $(".fonth5").toggleClass('fonth52');
        $(".personal-information").toggleClass('personal-information2');
        $(".personal-information h3").toggleClass('personal-information2 h3');
        $(".personal-information h5").toggleClass('personal-information2 h5');
        $(".accordion").toggleClass('accordion2');
        $(".flex-footer").toggleClass('flex-footer2');
    });
    
    // default books (quilting)
    $.ajax({
        method:"GET",
        url: "/tableofbooks?q=quilting",
        success: function(result){
            var data = result.data;
            for (i = 0; i < data.length; i++) {
                var image = data[i].image;
                var imageFinal = '<img' + ' width="80" height="100" src="' + image + '">';
                var title = data[i].title;
                var publishedDate = data[i].published;
                var id = data[i].id;
                var button = '<img id="' + id + '" width="25" height="25" src="/static/default-star.png" onclick="addFav(id)">';
                var html = '<tr>' + '<td>' + imageFinal + '</td>' + '<td>' + title + 
                '</td>' + '<td>' + publishedDate + '</td>' + '<td>' + button + '</td>' + '</tr>';
                $('#books-table').append(html);
            }
        },
        error: function(error){
            alert("Books not found");
        }
    });

});

// other books
var query = "quilting";
$(".submit").on("click", function () {
    query = this.id;
    $.ajax({
        method:"GET",
        url: "/tableofbooks?q="+query,
        success: function(result){
            $("td").remove();
            var data = result.data;
            for (i = 0; i < data.length; i++) {
                var image = data[i].image;
                var imageFinal = '<img' + ' width="80" height="100" src="' + image + '">';
                var title = data[i].title;
                var publishedDate = data[i].published;
                var id = data[i].id;
                var button = '<img id="' + id + '" width="25" height="25" src="/static/default-star.png" onclick="addFav(id)">';
                var html = '<tr>' + '<td>' + imageFinal + '</td>' + '<td>' + title + 
                '</td>' + '<td>' + publishedDate + '</td>' + '<td>' + button + '</td>' + '</tr>';
                $('#books-table').append(html);
            }
            $('#totalFav').replaceWith('<span id="totalFav">' + 0 + '</span>');
        },
        error: function(error){
            alert("Books not found");
        }
    });
});

// to add favorite
function addFav(id) {
    $.ajax({
        method:"GET",
        url: "/tableofbooks?q=" + query,
        success: function(result){
            var data = result.data;
            count = document.getElementById('totalFav').innerHTML;
            countFav = parseInt(count);
            console.log(countFav);
            for (i = 0; i < data.length; i++) {
                var id2 = data[i].id;
                if (id == id2) {
                    var img = document.getElementById(id);
                    if (img.src.match("/static/default-star.png")) {
                        img.src = "/static/yellow-star.png";
                        countFav++;
                    } else {
                        img.src = "/static/default-star.png";
                        countFav--;
                    }
                }
            }
            $('#totalFav').replaceWith('<span id="totalFav">' + countFav + '</span>');
        },
        error: function(error){
            alert("Books not found");
        }
    });
}

// Loader
var myVar;
function myFunction() {
    myVar = setTimeout(showPage, 2000);
}
function showPage() {
    document.getElementById("loader").style.display = "none";
    document.getElementById("myDiv").style.display = "block";
}


// STORY 10 (SUBSCRIBE)
$(function () {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    var email_available;
    var timer = 0;

    $('#email').keydown(function () {
        clearTimeout(timer);
        timer = setTimeout(checkValidEmail, 1000);
        warning();
    });

    $('#name').keydown(function () {
        clearTimeout(timer);
        timer = setTimeout(warning, 1000);
    });

    $('#password').keydown(function () {
        clearTimeout(timer);
        timer = setTimeout(warning, 1000);
    });

    $('#form').on('submit', function (event) {
        event.preventDefault();
        console.log("Form Submitted!");
        saveData();
    });

    function saveData() {
        $.ajax({
            method: 'POST',
            url: "post/",
            headers: {
                "X-CSRFToken": csrftoken,
            },
            data: {
                name: $('#name').val(),
                email: $('#email').val(),
                password: $('#password').val(),
            },
            success: function (response) {
                if (response.is_success) {
                    $('#name').val('');
                    $('#email').val('');
                    $('#password').val('');
                    $('#submit-btn').prop('disabled', true);
                    $('.error-message p').replaceWith("<p class='success'>You have successfully subscribed!</p>");
                    console.log("Successfully add data");
                } else {
                    $('.error-message p').replaceWith("<p class='fail'>Oops! Something went wrong!</p>");
                }
            },
            error: function () {
                alert("Error, cannot save data to database");
            }
        })
    }

    function checkValidEmail() {
        var reg = /^([a-zA-Z0-9_.+-])+@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var is_valid = reg.test($('#email').val());
        if (is_valid) {
            validateEmail();
        } else {
            $('.error-message p').replaceWith("<p class='fail'>Please enter a valid email format!</p>");
        }
    }

    function validateEmail() {
        $.ajax({
            method: 'POST',
            url: "validate/",
            headers: {
                "X-CSRFToken": csrftoken,
            },
            data: {
                email: $('#email').val(),
            },
            success: function (email) {
                if (email.is_exists) {
                    email_available = false;
                    $('.error-message p').replaceWith("<p class='fail'>Please use another email! This email is already been registered.</p>");
                } else {
                    email_available = true;
                    warning();
                }
            },
            error: function () {
                alert("Error, cannot validate email!")
            }
        })
    }

    function warning() {
        var password = $('#password').val();
        var name = $('#name').val();
        var email = $('#email').val();
        if (password.length !== 0 && name.length !== 0 && email_available) {
            $('.error-message p').replaceWith("<p></p>");
            $('#submit-btn').prop('disabled', false);
        } else if (password.length === 0 && name.length === 0) {
            $('#submit-btn').prop('disabled', true);
            $('.error-message p').replaceWith("<p class='fail'>Name and password cannot be empty</p>");
        } else if (password.length === 0 && email.length === 0) {
            $('#submit-btn').prop('disabled', true);
            $('.error-message p').replaceWith("<p class='fail'>Email and password cannot be empty</p>");
        } else if (name.length === 0 && email.length) {
            $('#submit-btn').prop('disabled', true);
            $('.error-message p').replaceWith("<p class='fail'>Name and email cannot be empty</p>");
        } else if (password.length === 0) {
            $('#submit-btn').prop('disabled', true);
            $('.error-message p').replaceWith("<p class='fail'>Password cannot be empty</p>");
        } else if (password.length < 6 && password.length > 12) {
            $('#submit-btn').prop('disabled', true);
            $('.error-message p').replaceWith("<p class='fail'>Password must contain 6-12 characters</p>");
        } else if (name.length === 0) {
            $('#submit-btn').prop('disabled', true);
            $('.error-message p').replaceWith("<p class='fail'>Name cannot be empty</p>");
        } else if (email.length === 0) {
            $('#submit-btn').prop('disabled', true);
            $('.error-message p').replaceWith("<p class='fail'>Email cannot be empty</p>");
        } else {
            $('#submit-btn').prop('disabled', true);
            $('.error-message p').replaceWith("<p class='fail'>Please enter a valid email format!</p>");
        }
    }
});


// CHALLENGE STORY 10
$(document).ready(function () {
    $.ajax({
        method:"GET",
        url: "/data/",
        success: function(subscriber_list) {
            for (i = 0; i < subscriber_list.length; i++) {
                var name = subscriber_list[i]['name'];
                var email = subscriber_list[i]['email'];
                var button = '<button class="btn btn-dark" id=' + email + ' onclick=unsubscribe(this)>' + 'unsubscribe' + '</button>';
                var html = '<tr>' + '<td>' + name + '</td>' + '<td>' + email + 
                '</td>' + '<td>' + button + '</td>' + '</tr>';
                $('#subs-table').append(html);
            }
        },
        error: function(error){
            alert("Subscriber's datas not found");
        }
    });

})

function unsubscribe(t) {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    var email = t.id;
    $.ajax({
        method: 'POST',
        url: "delete/",
        headers: {
            "X-CSRFToken": csrftoken
        },
        data: {email: email},
        success: function (res) {
            console.log('success');
            $('#subs-table').replaceWith("<tbody id='subs-table'></tbody>");
            var data = res.now;
            for (var i = 0; i < data.length; i++) {
                var name = data[i]['name'];
                var email = data[i]['email'];
                var button = '<button class="btn btn-dark" id=' + email + ' onclick=unsubscribe(this)>' + 'unsubscribe' + '</button>';
                var html = '<tr>' + '<td>' + name + '</td><td>' + email + '</td><td>' + button + '</td>';
                $('#subs-table').append(html)
            }
        }
    })            
}


// STORY 11 (LOGIN)
$(document).ready(function () {
    gapi.load('auth2', function() {
     gapi.auth2.init();
   });
 });

function onSignIn(googleUser) {
    var id_token = googleUser.getAuthResponse().id_token;
    console.log(id_token);
    sendToken(id_token);
    console.log("logged in")
}

function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
        console.log('User signed out.');
    });
}

var sendToken = function (token) {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    $.ajax({
        method: "POST",
        url: "/login/",
        headers: {
            "X-CSRFToken": csrftoken
        },
        data: {id_token: token},
        success: function (result) {
            console.log("Successfully logged in");
            if (result.status === "0") {
                html = "<h4 class='success'>Logged In</h4>";
                window.location.replace(result.url)
            } else {
                html = "<h4 class='fail'>Something error, please report</h4>"
            }
            $("h4").replaceWith(html)
        },
        error: function () {
            alert("Something error, please report")
        }
    })
};

