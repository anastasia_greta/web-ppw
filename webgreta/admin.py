from django.contrib import admin
from .models import StatusModel, SubscribeModel

# Story 6 (Status)
admin.site.register(StatusModel)

# Story 10 (Subscribe)
admin.site.register(SubscribeModel)
