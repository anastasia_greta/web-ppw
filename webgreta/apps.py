from django.apps import AppConfig


class WebgretaConfig(AppConfig):
    name = 'webgreta'
