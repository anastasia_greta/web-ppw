from django import forms

# Story 6 (Status)
class StatusForm(forms.Form):
    error_messages = {
        'required': 'Please input this form',
    }
    message_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder':'What is on your mind?'
    }

    message = forms.CharField(label='', required=True, max_length=300, widget=forms.Textarea(attrs=message_attrs))


# Story 10 (Subscribe)
class SubscribeForm(forms.Form):
    name_attrs = {
        'id': 'name',
        'class' : 'form-control',
        'placeholder': 'Enter your full name'
    }
    email_attrs = {
        'id': 'email',
        'class' : 'form-control',
        'placeholder': 'Enter a valid email address'
    }
    password_attrs = {
        'id': 'password',
        'class' : 'form-control',
        'placeholder': 'Enter your password'
    }
    name = forms.CharField(label="Full Name", required=True, max_length=100, widget=forms.TextInput(attrs=name_attrs))
    email = forms.EmailField(label="Email", required=True, max_length=100, widget=forms.TextInput(attrs=email_attrs))
    password = forms.CharField(label="Password", required=True, min_length=6, max_length=12, widget=forms.PasswordInput(attrs=password_attrs))

