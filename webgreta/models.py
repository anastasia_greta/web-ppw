from django.db import models

# Story 6 (Status)
class StatusModel(models.Model):
    message = models.CharField(max_length=300)
    created_at = models.DateTimeField(auto_now_add=True)


# Story 10 (Subscribe)
class SubscribeModel(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField(max_length=100, unique=True, primary_key=True)
    password = models.CharField(max_length=12)
