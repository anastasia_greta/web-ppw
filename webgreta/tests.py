from django.db import IntegrityError
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.utils.datastructures import MultiValueDictKeyError
from .views import landingpage, profile, index, tableofbooks, subscribe, subscriber, list_data, delete_data
from .models import StatusModel, SubscribeModel
from .forms import StatusForm, SubscribeForm

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest
import time
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

# Create your tests here.
class Lab6UnitTest(TestCase):

    # TDD story 6 (status page)
    def test_lab_6_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_lab_6_using_index_function(self):
        found = resolve('/')
        self.assertEqual(found.func, landingpage)

    def test_model_can_add_new_status(self):
        message = StatusModel.objects.create(message='PPW is fun')
        counting_all_message = StatusModel.objects.all().count()
        self.assertEqual(counting_all_message, 1)

    def test_if_form_is_blank(self):
        form = StatusForm(data={'message': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['message'][0],
            'This field is required.',
        )
    
    def test_form_todo_input_has_placeholder_and_css_classes(self):
        form = StatusForm()
        self.assertIn('class="form-control', form.as_p())

    def test_form_post_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/fill_status/', {'message': test})
        self.assertEqual(response_post.status_code, 302)
        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_form_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/fill_status/', {'message': ''})
        self.assertEqual(response_post.status_code, 302)
        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)


    ## TDD for profile page
    def test_profile_url_is_exist(self):
        response = Client().get('/webgreta/profile')
        self.assertEqual(response.status_code, 200)
    
    def test_profile_using_index_function(self):
        found = resolve('/webgreta/profile')
        self.assertEqual(found.func, profile)
    
    def test_profile_using_to_do_list_template(self):
        response = Client().get('/webgreta/profile')
        self.assertTemplateUsed(response, 'profile.html')


    # TDD story 9 (books page)
    # def test_tablebooks_url_is_exist(self):
    #     response = Client().get('/webgreta/tableofbooks?q=quilting')
    #     self.assertEqual(response.status_code, 200)

    # def test_index_url_is_exist(self):
    #     response = Client().get('/webgreta/index')
    #     self.assertEqual(response.status_code, 200)

    def test_url_main_page_redirect_to_other_page(self):
        response = Client().get('/webgreta/tableofbooks?q=quilting')
        self.assertEqual(response.status_code, 302)
    
    def test_index_using_index_function(self):
        found = resolve('/webgreta/index')
        self.assertEqual(found.func, index)

    
    # TDD story 10 (subscription form)
    def test_subcribe_page_url_is_exist(self):
        response = Client().get('/webgreta/subscribe')
        self.assertEqual(response.status_code, 200)
    
    def test_subscribe_using_function(self):
        found = resolve('/webgreta/subscribe')
        self.assertEqual(found.func, subscribe)

    def test_model_can_save_data_form(self):
        data = SubscribeModel.objects.create(name="aku", email="aku@gmail.com", password="aku123")
        counting_all_data = SubscribeModel.objects.all().count()
        self.assertEqual(counting_all_data, 1)
    
    def test_self_func_name(self):
        SubscribeModel.objects.create(name='aku', email='aku@gmail.com', password='aku123')
        subscriber = SubscribeModel.objects.get(email='aku@gmail.com')
        self.assertEqual('aku@gmail.com', subscriber.email)

    def test_email_must_unique(self):
        SubscribeModel.objects.create(email="aku@gmail.com")
        with self.assertRaises(IntegrityError):
            SubscribeModel.objects.create(email="aku@gmail.com")

    def test_post_using_ajax(self):
        response = Client().post('/post/', data={
            "name": "aku",
            "email": "aku@gmail.com",
            "password": "aku123",
        })
        self.assertEqual(response.status_code, 200)

    def test_check_email_view_return_200(self):
        response = Client().post('/validate/', data={
            "email": "aku@gmail.com"
        })
        self.assertEqual(response.status_code, 200)
    
    def test_check_email_already_exist_view_return_200(self):
        SubscribeModel.objects.create(name="aku",
                                      email="aku@gmail.com",
                                      password="aku123")
        response = Client().post('/validate/', data={
            "email": "aku@gmail.com"
        })
        self.assertEqual(response.json()['is_exists'], True)

    def test_if_subform_is_blank(self):
        form = SubscribeForm(data={
            "name": "",
            "email": "",
            "password": "",
        })
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['name'][0],
            'This field is required.',
        )

    def test_subform_todo_input_has_placeholder_and_css_classes(self):
        form = SubscribeForm()
        self.assertIn('class="form-control', form.as_p())
    

    # Challenge story 10
    def test_url_subscriber_page_is_exist(self):
        response = Client().get('/webgreta/listSubscriber')
        self.assertEqual(response.status_code, 200)

    def test_url_subscriber_using_function(self):
        found = resolve('/webgreta/listSubscriber')
        self.assertEqual(found.func, subscriber)

    def test_url_data_page_is_exist(self):
        response = Client().get('/webgreta/data/')
        self.assertEqual(response.status_code, 200)

    def test_url_data_using_function(self):
        found = resolve('/webgreta/data/')
        self.assertEqual(found.func, list_data)

    def test_url_delete_page_is_exist(self):
        response = Client().post('/webgreta/delete/', data={
            "email": "emailku@gmail.com"
        })
        self.assertEqual(response.status_code, 200)

    def test_url_delete_using_function(self):
        found = resolve('/webgreta/delete/')
        self.assertEqual(found.func, delete_data)
 

# class Lab6FunctionalTest(unittest.TestCase):

#     def setUp(self):
#         capabilities = {
#             'browserName': 'chrome',
#             'chromeOptions': {
#                 'useAutomationExtension': False,
#                 'forceDevToolsScreenshot': True,
#                 'args': ['--headless', '--no-sandbox', 'disable-dev-shm-usage']
#             }
#         }
#         self.browser = webdriver.Chrome('./chromedriver', desired_capabilities=capabilities)
#         super(Lab6FunctionalTest, self).setUp()

#     def tearDown(self):
#         self.browser.quit()
#         super(Lab6FunctionalTest, self).tearDown()

#     def test_input_todo(self):
#         self.browser.get('http://webgreta.herokuapp.com')
#         time.sleep(5) # Let the user actually see something!
#         message = self.browser.find_element_by_id('id_message')
#         message.send_keys('kuliah menyenangkan tapi sangaaat melelahkan')
#         message.submit()
#         time.sleep(5) # Let the user actually see something!
#         self.assertIn("kuliah menyenangkan tapi sangaaat melelahkan", self.browser.page_source)
#         time.sleep(10) # Let the user actually see something!
    
#     def test_header_text_layout(self):
#         self.browser.get('http://webgreta.herokuapp.com')
#         header_text = self.browser.find_element_by_tag_name('h1').text
#         self.assertIn("Hello! How are you?", header_text)
    
#     def test_title_layout(self):
#         self.browser.get('http://webgreta.herokuapp.com')
#         title = self.browser.find_element_by_tag_name('h4').text
#         self.assertIn("Insert your status here!", title)
    
#     def test_button_style(self):
#         self.browser.get('http://webgreta.herokuapp.com')
#         button_color = self.browser.find_element_by_css_selector('button[type="submit"]')
#         self.assertEqual('btn btn-primary', button_color.get_attribute('class'))
    
#     def test_box_style(self):
#         self.browser.get('http://webgreta.herokuapp.com')
#         result_box = self.browser.find_element_by_class_name('result-box')
#         color = result_box.value_of_css_property('background-color')
#         self.assertEqual('rgba(255, 255, 255, 0.7)', color)

#     # def test_accordion(self):
#     #     self.browser.get('http://127.0.0.1:8000/webgreta')
#     #     accord_text = self.browser.find_element_by_id('pengalaman').text
#     #     self.assertIn("Experiences", accord_text)
    
# if __name__ == '__main__' : #
#     unittest.main(warnings='ignore') #


