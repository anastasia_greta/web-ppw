from django.urls import path
from .views import landingpage, fill_status, profile, tableofbooks, index
from .views import subscribe, post, validate_email, list_data, subscriber, delete_data
from .views import login, logout

urlpatterns = [
     path('landingpage', landingpage, name="home"),
    path('fill_status/', fill_status, name='fill_status'),
    path('profile', profile, name='profile'),
    path('tableofbooks', tableofbooks),
    path('index', index, name='index'),
    path('subscribe', subscribe, name="subscribe"),
    path('validate/', validate_email, name="validate"),
    path('post/', post, name="post"),
    path('listSubscriber', subscriber, name="subscriber"),
    path('data/', list_data, name="data"),
    path('delete/', delete_data, name="delete"),
    path('login/', login, name="login"),
    path('logout/', logout, name="logout"),
    path('', landingpage)
]