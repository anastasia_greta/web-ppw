from django.shortcuts import render
from google.oauth2 import id_token
from google.auth.transport import requests as google_requests
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from .forms import StatusForm, SubscribeForm
from .models import StatusModel, SubscribeModel
import requests
import json

# Create your views here.

response = {}

# Story 6 (Status page)
def landingpage(request):
    statusku = StatusModel.objects.all()
    response['statusku'] = statusku
    response['status_form'] = StatusForm
    return render(request, 'landingpage.html', response)

def fill_status(request):
    form = StatusForm(request.POST or None)
    if request.method == 'POST' and form.is_valid():
        response['status_message'] = request.POST['message']
        statusku = StatusModel(message=response['status_message'])
        statusku.save()
        return HttpResponseRedirect('/')
    else:
        return HttpResponseRedirect('/')


# Profile page
def profile(request):
    return render(request, 'profile.html')



# Story 9 (Books Collection)
def tableofbooks(request):
    if 'user_id' not in request.session.keys():
        return HttpResponseRedirect(reverse('login'))
    query = request.GET['q']
    url = "https://www.googleapis.com/books/v1/volumes?q=" + query
    json_src = requests.get(url).json()
    result = json_src['items']
    books_list = []
    for items in result:
        data = items['id']
        books_data = {"image": items['volumeInfo']['imageLinks']['thumbnail'], "title": items['volumeInfo']['title'],
                        "published": items['volumeInfo']['publishedDate'], 'id': items['id']}
        books_list.append(books_data)
    return JsonResponse({'data': books_list})

def index(request):
    # if 'user_id' not in request.session.keys():
    #     return HttpResponseRedirect(reverse('login'))
    # if 'books' in request.session.keys():
    #     list_book = request.session['books']
    # else:
    #     list_book = []
    response['name'] = request.session['name']
    # response['count'] = len(list_book)
    return render(request, 'tableofbooks.html', response)

# def add_fav(request):
#     if 'user_id' not in request.session:
#         return HttpResponseRedirect(reverse('login'))
#     if request.method == 'POST':
#         book_id = request.POST['id']
#         if 'books' not in request.session.keys():
#             request.session['books'] = [book_id]
#             size = 1
#         else:
#             books = request.session['books']
#             books.append(book_id)
#             request.session['books'] = books
#             size = len(books)
#         return JsonResponse({'count': size, 'id': book_id})

# def remove_fav(request):
#     if 'user_id' not in request.session:
#         return HttpResponseRedirect(reverse('login'))
#     if request.method == 'POST':
#         book_id = request.POST['id']
#         books = request.session['books']
#         books.remove(book_id)
#         request.session['books'] = books
#         size = len(books)
#         return JsonResponse({'count': size, 'id': book_id})

    

# Story 10 (Subcription Form)
def subscribe(request):
    form = SubscribeForm(request.POST or None)
    response['form'] = form
    return render(request, 'subscribe.html', response)

def post(request):
    form = SubscribeForm(request.POST or None)
    if request.method == 'POST' and form.is_valid():
        name = request.POST['name']
        email = request.POST['email']
        password = request.POST['password']
        SubscribeModel.objects.create(name=name, email=email, password=password)
        return JsonResponse({'is_success': True})

@csrf_exempt
def validate_email(request):
    if request.method == 'POST':
        email = request.POST['email']
        check_email = SubscribeModel.objects.filter(email=email)
        if check_email.exists():
            return JsonResponse({'is_exists': True})
        return JsonResponse({'is_exists': False})


# Challenge story 10
def subscriber(request):
    return render(request, 'listSubscriber.html')

def list_data(request):
    lists = SubscribeModel.objects.all().values()
    subscriber_list = list(lists)
    return JsonResponse(subscriber_list, safe=False)

def delete_data(request):
    if request.method == 'POST':
        email = request.POST['email']
        SubscribeModel.objects.filter(email=email).delete()
        now = list(SubscribeModel.objects.all().values())
        return JsonResponse({'deleted': True, 'now': now})



# Story 11 (log in)
def login(request):
    if request.method == "POST":
        try:
            token = request.POST['id_token']
            id_info = id_token.verify_oauth2_token(token, google_requests.Request(),
                                                  "76022581806-o1oir0846d7f5hl5v18gba00evral527.apps.googleusercontent.com")
            if id_info['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
                raise ValueError('Wrong issuer.')
            userid = id_info['sub']
            email = id_info['email']
            name = id_info['name']
            request.session['user_id'] = userid
            request.session['email'] = email
            request.session['name'] = name
            request.session['book'] = []
            return JsonResponse({"status": "0", "url": reverse("index")})
        except ValueError:
            return JsonResponse({"status": "1"})
    return render(request, 'login.html')

def logout(request):
    request.session.flush()
    return HttpResponseRedirect(reverse('login'))
